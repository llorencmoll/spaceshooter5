﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject Enemy;
	float maxSpawnRate = 5f;

	// Use this for initialization
	void Start () {
		Invoke ("SpawnEnemy", maxSpawnRate);

		InvokeRepeating ("IncreaseSpawn", 0f, 30f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy()
	{
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));

		GameObject InsEnemy = (GameObject)Instantiate (Enemy);
		InsEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);

		NextEnemySpawn();
	}

	void NextEnemySpawn()
	{
		float SpawnInSeconds;

		if (maxSpawnRate > 1f) {
			SpawnInSeconds = Random.Range (1f, maxSpawnRate);
		} else
			SpawnInSeconds = 1f;
		Invoke ("SpawnEnemy", SpawnInSeconds);
	}

	void IncreaseSpawn()
	{
		if (maxSpawnRate > 1f)
			maxSpawnRate--;
		if (maxSpawnRate == 1f)
			CancelInvoke ("IncreaseSpawn");
	}
}
