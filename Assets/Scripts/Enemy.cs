﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public GameObject Explosion;


	float speed;

    public int HighscoreValue;
    private MyGameManager GM;

	// Use this for initialization
	void Start () {

		speed = 2f;
        GameObject GMObject = GameObject.FindWithTag("GameController");
        GM = GMObject.GetComponent<MyGameManager>();
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 position = transform.position;
		position = new Vector2 (position.x, position.y - speed * Time.deltaTime);
		transform.position = position;

		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));

		if (transform.position.y < min.y) {
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if ((col.tag == "PlayerShipTag") || (col.tag == "Bullet")) {
			PlayExplosion ();

            GM.AddHighscore(HighscoreValue);

            Destroy(gameObject);
		}
	}

	void PlayExplosion()
	{
		GameObject explosion = (GameObject)Instantiate (Explosion);
		explosion.transform.position = transform.position;
	}
}
