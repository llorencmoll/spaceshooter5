﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour {

	public Text HighscoreText;
	public Text LivesText;
	private int lives;
	private int Highscore;

	private static MyGameManager instance;

	void Awake (){
		if (instance == null) {
			instance = this;
		}
	}

	public static MyGameManager getInstance(){
		return instance;
	}


	// Use this for initialization
	void Start () {
		lives = 3;
		Highscore = 0;
        //HighscoreText.text = "x " + Highscore.ToString("D5");
        LivesText.text = "x " + lives.ToString ();
		
	}

	public void LoseLive () {
		lives--;
		LivesText.text = "x " + lives.ToString ();

		if (lives <= -1) {
			SceneManager.LoadScene ("GameOver");
		}
		
	}

	public void AddHighscore (int value){
        Highscore += value;
        UpdateHighscore();

    }

    void UpdateHighscore()
    {
        HighscoreText.text = "Score: " + Highscore;
    }
}
